﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockManagementApp.Objets
{
    class ProduitBD
    {
        private StockEntities DB=new StockEntities();
        private Produit Pr;

        public bool AjouterProduit(int CategorieId, string Nom, double PrixUnitaire, byte[] ProduitImage, int Nombre, DateTime DateExpiration)
        {
            Pr = new Produit();
            Pr.Categorie_Id = CategorieId;
            Pr.Nom = Nom;
            Pr.Prix_Unitaire = (decimal)PrixUnitaire;
            Pr.Poduit_Image = ProduitImage;
            Pr.Nombre = Nombre;
            Pr.Date_Creation = DateTime.Now;
            Pr.Date_Expiration = DateExpiration;         
            if (DB.Produit.SingleOrDefault(p => p.Nom == Nom) == null)
            {
                
                DB.Produit.Add(Pr);
                DB.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public void ModifierProduit(int idp,int CategorieId, string Nom, double PrixUnitaire, byte[] ProduitImage, int Nombre, DateTime DateExpiration)
        {
            Pr = new Produit();
            Pr = DB.Produit.SingleOrDefault(s => s.Id == idp);
            if (Pr != null)
            {
                Pr.Categorie_Id = CategorieId;
                Pr.Nom = Nom;
                Pr.Prix_Unitaire = (decimal)PrixUnitaire;
                Pr.Poduit_Image = ProduitImage;
                Pr.Nombre = Nombre;
                Pr.Date_Creation = DateTime.Now;
                Pr.Date_Expiration = DateExpiration;
                DB.SaveChanges();
            }

        }
        
    
        public void SupprimerProduit(int id)
        {
            Pr = new Produit();
            Pr = DB.Produit.SingleOrDefault(s => s.Id == id);
            if (Pr != null)
            {
                DB.Produit.Remove(Pr);
                DB.SaveChanges();
            }
        }
    }
}

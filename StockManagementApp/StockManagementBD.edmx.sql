
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/05/2021 15:37:29
-- Generated from EDMX file: C:\Users\DELL\Desktop\stockmanagement-produit-master\StockManagementApp\StockManagementBD.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [gestion_de_stock];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Achat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Produit_Achat] DROP CONSTRAINT [FK_Achat];
GO
IF OBJECT_ID(N'[dbo].[FK_Produit_Categorie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Produit] DROP CONSTRAINT [FK_Produit_Categorie];
GO
IF OBJECT_ID(N'[dbo].[FK_Produit]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Produit_Achat] DROP CONSTRAINT [FK_Produit];
GO
IF OBJECT_ID(N'[dbo].[FK_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Produit_Achat] DROP CONSTRAINT [FK_Client];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Achat]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Achat];
GO
IF OBJECT_ID(N'[dbo].[Categorie]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Categorie];
GO
IF OBJECT_ID(N'[dbo].[Client]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Client];
GO
IF OBJECT_ID(N'[dbo].[Produit]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Produit];
GO
IF OBJECT_ID(N'[dbo].[Produit_Achat]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Produit_Achat];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Achat'
CREATE TABLE [dbo].[Achat] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date_Vente] datetime  NOT NULL,
    [Date_Creation] datetime  NOT NULL
);
GO

-- Creating table 'Categorie'
CREATE TABLE [dbo].[Categorie] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nom] nchar(255)  NOT NULL
);
GO

-- Creating table 'Client'
CREATE TABLE [dbo].[Client] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nom] nchar(255)  NOT NULL,
    [Prenom] nchar(255)  NOT NULL,
    [Adresse] nchar(255)  NOT NULL,
    [Tel] nchar(255)  NOT NULL,
    [Pays] nchar(255)  NOT NULL,
    [Ville] nchar(255)  NOT NULL
);
GO

-- Creating table 'Produit'
CREATE TABLE [dbo].[Produit] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Categorie_Id] int  NOT NULL,
    [Nom] nchar(255)  NOT NULL,
    [Prix_Unitaire] decimal(18,0)  NOT NULL,
    [Poduit_Image] varbinary(max)  NOT NULL,
    [Nombre] int  NOT NULL,
    [Date_Expiration] datetime  NOT NULL,
    [Date_Creation] datetime  NOT NULL
);
GO

-- Creating table 'Produit_Achat'
CREATE TABLE [dbo].[Produit_Achat] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Produit_Id] int  NOT NULL,
    [Achat_Id] int  NOT NULL,
    [Client_Id] int  NOT NULL,
    [Quantite] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Achat'
ALTER TABLE [dbo].[Achat]
ADD CONSTRAINT [PK_Achat]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Categorie'
ALTER TABLE [dbo].[Categorie]
ADD CONSTRAINT [PK_Categorie]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Client'
ALTER TABLE [dbo].[Client]
ADD CONSTRAINT [PK_Client]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Produit'
ALTER TABLE [dbo].[Produit]
ADD CONSTRAINT [PK_Produit]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Produit_Achat'
ALTER TABLE [dbo].[Produit_Achat]
ADD CONSTRAINT [PK_Produit_Achat]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Achat_Id] in table 'Produit_Achat'
ALTER TABLE [dbo].[Produit_Achat]
ADD CONSTRAINT [FK_Achat]
    FOREIGN KEY ([Achat_Id])
    REFERENCES [dbo].[Achat]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Achat'
CREATE INDEX [IX_FK_Achat]
ON [dbo].[Produit_Achat]
    ([Achat_Id]);
GO

-- Creating foreign key on [Categorie_Id] in table 'Produit'
ALTER TABLE [dbo].[Produit]
ADD CONSTRAINT [FK_Produit_Categorie]
    FOREIGN KEY ([Categorie_Id])
    REFERENCES [dbo].[Categorie]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Produit_Categorie'
CREATE INDEX [IX_FK_Produit_Categorie]
ON [dbo].[Produit]
    ([Categorie_Id]);
GO

-- Creating foreign key on [Produit_Id] in table 'Produit_Achat'
ALTER TABLE [dbo].[Produit_Achat]
ADD CONSTRAINT [FK_Produit]
    FOREIGN KEY ([Produit_Id])
    REFERENCES [dbo].[Produit]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Produit'
CREATE INDEX [IX_FK_Produit]
ON [dbo].[Produit_Achat]
    ([Produit_Id]);
GO

-- Creating foreign key on [Client_Id] in table 'Produit_Achat'
ALTER TABLE [dbo].[Produit_Achat]
ADD CONSTRAINT [FK_Client]
    FOREIGN KEY ([Client_Id])
    REFERENCES [dbo].[Client]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Client'
CREATE INDEX [IX_FK_Client]
ON [dbo].[Produit_Achat]
    ([Client_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
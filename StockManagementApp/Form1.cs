﻿using StockManagementApp.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockManagementApp
{
    public partial class StockManagenemtApp : Form
    {
        public StockManagenemtApp()
        {
            InitializeComponent();
        }

        private void dashboardBtn_Click(object sender, EventArgs e)
        {
            sidePanel.Height = dashboardBtn.Height;
            sidePanel.Top = dashboardBtn.Top;
        }

        private void produitsBtn_Click(object sender, EventArgs e)
        {
            sidePanel.Height = produitsBtn.Height;
            sidePanel.Top = produitsBtn.Top;
     
           
            if (!panelAfficheProduit.Controls.Contains(ProduitsForm.Instance))
            {
                ProduitsForm.Instance.TopLevel = false;

                panelAfficheProduit.Controls.Add(ProduitsForm.Instance);
                ProduitsForm.Instance.Dock = DockStyle.Fill;
                ProduitsForm.Instance.BringToFront();
                ProduitsForm.Instance.Show();
            }
            else
            {
                ProduitsForm.Instance.BringToFront();
            }

        }

        private void fournisseursBtn_Click(object sender, EventArgs e)
        {
            sidePanel.Height = fournisseursBtn.Height;
            sidePanel.Top = fournisseursBtn.Top;
        }

        private void usersBtn_Click(object sender, EventArgs e)
        {
            sidePanel.Height = usersBtn.Height;
            sidePanel.Top = usersBtn.Top;
        }
        private void aProposBtn_Click(object sender, EventArgs e)
        {
            sidePanel.Height = aProposBtn.Height;
            sidePanel.Top = aProposBtn.Top;
        }

        private void iconButton5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*
        private void btFormProduit_Click(object sender, EventArgs e)
        {
            StockManagementApp.Forms.ProduitsForm form = new StockManagementApp.Forms.ProduitsForm();
            form.ShowDialog();
        }
        */
    }
}

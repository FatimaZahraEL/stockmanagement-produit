﻿
namespace StockManagementApp.Forms
{
    partial class AjouterProduitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.cbProduitCategorie = new System.Windows.Forms.ComboBox();
            this.dateExpitation = new MetroFramework.Controls.MetroDateTime();
            this.tbProduitNombre = new MetroFramework.Controls.MetroTextBox();
            this.tbProduitPU = new MetroFramework.Controls.MetroTextBox();
            this.tbProduitNom = new MetroFramework.Controls.MetroTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btParcourir = new MetroFramework.Controls.MetroButton();
            this.imageProduit = new System.Windows.Forms.PictureBox();
            this.LabelTitre = new System.Windows.Forms.Label();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageProduit)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.White;
            this.metroPanel1.Controls.Add(this.cbProduitCategorie);
            this.metroPanel1.Controls.Add(this.dateExpitation);
            this.metroPanel1.Controls.Add(this.tbProduitNombre);
            this.metroPanel1.Controls.Add(this.tbProduitPU);
            this.metroPanel1.Controls.Add(this.tbProduitNom);
            this.metroPanel1.Controls.Add(this.label6);
            this.metroPanel1.Controls.Add(this.label5);
            this.metroPanel1.Controls.Add(this.label4);
            this.metroPanel1.Controls.Add(this.label3);
            this.metroPanel1.Controls.Add(this.label2);
            this.metroPanel1.Controls.Add(this.btParcourir);
            this.metroPanel1.Controls.Add(this.imageProduit);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(23, 63);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(571, 252);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // cbProduitCategorie
            // 
            this.cbProduitCategorie.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProduitCategorie.FormattingEnabled = true;
            this.cbProduitCategorie.Location = new System.Drawing.Point(304, 51);
            this.cbProduitCategorie.Name = "cbProduitCategorie";
            this.cbProduitCategorie.Size = new System.Drawing.Size(250, 29);
            this.cbProduitCategorie.TabIndex = 15;
            // 
            // dateExpitation
            // 
            this.dateExpitation.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateExpitation.FontWeight = MetroFramework.MetroDateTimeWeight.Bold;
            this.dateExpitation.Location = new System.Drawing.Point(304, 147);
            this.dateExpitation.MinimumSize = new System.Drawing.Size(0, 29);
            this.dateExpitation.Name = "dateExpitation";
            this.dateExpitation.Size = new System.Drawing.Size(250, 29);
            this.dateExpitation.TabIndex = 14;
            // 
            // tbProduitNombre
            // 
            // 
            // 
            // 
            this.tbProduitNombre.CustomButton.Image = null;
            this.tbProduitNombre.CustomButton.Location = new System.Drawing.Point(228, 1);
            this.tbProduitNombre.CustomButton.Name = "";
            this.tbProduitNombre.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbProduitNombre.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbProduitNombre.CustomButton.TabIndex = 1;
            this.tbProduitNombre.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbProduitNombre.CustomButton.UseSelectable = true;
            this.tbProduitNombre.CustomButton.Visible = false;
            this.tbProduitNombre.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.tbProduitNombre.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.tbProduitNombre.Lines = new string[0];
            this.tbProduitNombre.Location = new System.Drawing.Point(304, 118);
            this.tbProduitNombre.MaxLength = 32767;
            this.tbProduitNombre.Name = "tbProduitNombre";
            this.tbProduitNombre.PasswordChar = '\0';
            this.tbProduitNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbProduitNombre.SelectedText = "";
            this.tbProduitNombre.SelectionLength = 0;
            this.tbProduitNombre.SelectionStart = 0;
            this.tbProduitNombre.ShortcutsEnabled = true;
            this.tbProduitNombre.Size = new System.Drawing.Size(250, 23);
            this.tbProduitNombre.TabIndex = 13;
            this.tbProduitNombre.UseSelectable = true;
            this.tbProduitNombre.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbProduitNombre.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbProduitPU
            // 
            // 
            // 
            // 
            this.tbProduitPU.CustomButton.Image = null;
            this.tbProduitPU.CustomButton.Location = new System.Drawing.Point(228, 1);
            this.tbProduitPU.CustomButton.Name = "";
            this.tbProduitPU.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbProduitPU.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbProduitPU.CustomButton.TabIndex = 1;
            this.tbProduitPU.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbProduitPU.CustomButton.UseSelectable = true;
            this.tbProduitPU.CustomButton.Visible = false;
            this.tbProduitPU.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.tbProduitPU.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.tbProduitPU.Lines = new string[0];
            this.tbProduitPU.Location = new System.Drawing.Point(304, 86);
            this.tbProduitPU.MaxLength = 32767;
            this.tbProduitPU.Name = "tbProduitPU";
            this.tbProduitPU.PasswordChar = '\0';
            this.tbProduitPU.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbProduitPU.SelectedText = "";
            this.tbProduitPU.SelectionLength = 0;
            this.tbProduitPU.SelectionStart = 0;
            this.tbProduitPU.ShortcutsEnabled = true;
            this.tbProduitPU.Size = new System.Drawing.Size(250, 23);
            this.tbProduitPU.TabIndex = 12;
            this.tbProduitPU.UseSelectable = true;
            this.tbProduitPU.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbProduitPU.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tbProduitNom
            // 
            // 
            // 
            // 
            this.tbProduitNom.CustomButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbProduitNom.CustomButton.ForeColor = System.Drawing.Color.Black;
            this.tbProduitNom.CustomButton.Image = null;
            this.tbProduitNom.CustomButton.Location = new System.Drawing.Point(228, 1);
            this.tbProduitNom.CustomButton.Name = "";
            this.tbProduitNom.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.tbProduitNom.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tbProduitNom.CustomButton.TabIndex = 1;
            this.tbProduitNom.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tbProduitNom.CustomButton.UseSelectable = true;
            this.tbProduitNom.CustomButton.Visible = false;
            this.tbProduitNom.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.tbProduitNom.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.tbProduitNom.ForeColor = System.Drawing.Color.Black;
            this.tbProduitNom.Lines = new string[0];
            this.tbProduitNom.Location = new System.Drawing.Point(304, 22);
            this.tbProduitNom.MaxLength = 32767;
            this.tbProduitNom.Name = "tbProduitNom";
            this.tbProduitNom.PasswordChar = '\0';
            this.tbProduitNom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbProduitNom.SelectedText = "";
            this.tbProduitNom.SelectionLength = 0;
            this.tbProduitNom.SelectionStart = 0;
            this.tbProduitNom.ShortcutsEnabled = true;
            this.tbProduitNom.Size = new System.Drawing.Size(250, 23);
            this.tbProduitNom.TabIndex = 11;
            this.tbProduitNom.UseSelectable = true;
            this.tbProduitNom.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tbProduitNom.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(161, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Date d\'Expiration :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(161, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Nombre :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(161, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Prix Unitaire :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(161, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Categorie :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(161, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nom :";
            // 
            // btParcourir
            // 
            this.btParcourir.BackColor = System.Drawing.Color.DimGray;
            this.btParcourir.Location = new System.Drawing.Point(17, 168);
            this.btParcourir.Name = "btParcourir";
            this.btParcourir.Size = new System.Drawing.Size(138, 29);
            this.btParcourir.TabIndex = 3;
            this.btParcourir.Text = "Parcourir";
            this.btParcourir.UseSelectable = true;
            this.btParcourir.Click += new System.EventHandler(this.btParcourir_Click);
            // 
            // imageProduit
            // 
            this.imageProduit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageProduit.Location = new System.Drawing.Point(17, 18);
            this.imageProduit.Name = "imageProduit";
            this.imageProduit.Size = new System.Drawing.Size(138, 144);
            this.imageProduit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageProduit.TabIndex = 2;
            this.imageProduit.TabStop = false;
            // 
            // LabelTitre
            // 
            this.LabelTitre.AutoSize = true;
            this.LabelTitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTitre.ForeColor = System.Drawing.Color.Maroon;
            this.LabelTitre.Location = new System.Drawing.Point(197, 19);
            this.LabelTitre.Name = "LabelTitre";
            this.LabelTitre.Size = new System.Drawing.Size(208, 31);
            this.LabelTitre.TabIndex = 1;
            this.LabelTitre.Text = "Ajouter Produit";
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.BackColor = System.Drawing.Color.DimGray;
            this.btEnregistrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEnregistrer.Location = new System.Drawing.Point(436, 321);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(116, 36);
            this.btEnregistrer.TabIndex = 16;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = false;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // btAnnuler
            // 
            this.btAnnuler.BackColor = System.Drawing.Color.DimGray;
            this.btAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAnnuler.Location = new System.Drawing.Point(304, 321);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(126, 36);
            this.btAnnuler.TabIndex = 2;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = false;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // AjouterProduitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 376);
            this.Controls.Add(this.btEnregistrer);
            this.Controls.Add(this.btAnnuler);
            this.Controls.Add(this.LabelTitre);
            this.Controls.Add(this.metroPanel1);
            this.Name = "AjouterProduitForm";
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageProduit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public MetroFramework.Controls.MetroButton btParcourir;
        public System.Windows.Forms.PictureBox imageProduit;
        public System.Windows.Forms.ComboBox cbProduitCategorie;
        public MetroFramework.Controls.MetroDateTime dateExpitation;
        public MetroFramework.Controls.MetroTextBox tbProduitNombre;
        public MetroFramework.Controls.MetroTextBox tbProduitPU;
        public MetroFramework.Controls.MetroTextBox tbProduitNom;
        public System.Windows.Forms.Label LabelTitre;
        public System.Windows.Forms.Button btEnregistrer;
        public System.Windows.Forms.Button btAnnuler;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace StockManagementApp.Forms
{
    public partial class AjouterProduitForm : MetroFramework.Forms.MetroForm
    {
        private StockEntities DB;
        private ProduitsForm produitSection;

        public AjouterProduitForm(ProduitsForm product)
        {
            InitializeComponent();
            DB = new StockEntities();
            this.produitSection = product;
            cbProduitCategorie.DataSource = DB.Categorie.ToList();
            cbProduitCategorie.DisplayMember = "Nom";
            cbProduitCategorie.ValueMember = "Id";
        }

        private void btParcourir_Click(object sender, EventArgs e)
        {
            OpenFileDialog Op = new OpenFileDialog();
            Op.Filter = "|*.JPG;*.JPEG;*.PNG;*.GIF;*.BMP";
            if (Op.ShowDialog() == DialogResult.OK)
            {
                imageProduit.Image = Image.FromFile(Op.FileName);
            }
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            tbProduitNom.Text = string.Empty;
            tbProduitNombre.Text = string.Empty;
            tbProduitPU.Text = string.Empty;
            cbProduitCategorie.Text = string.Empty;
            dateExpitation.Text = string.Empty;
            imageProduit.Image = null;
            
        }

        public int IDProduit;
        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (LabelTitre.Text == "Ajouter Produit")
            {
                
                Objets.ProduitBD ClsProduit = new Objets.ProduitBD();
                MemoryStream MS = new MemoryStream();
                imageProduit.Image.Save(MS, imageProduit.Image.RawFormat);
                byte[] byteImage = MS.ToArray();
                if (ClsProduit.AjouterProduit(Convert.ToInt32(cbProduitCategorie.SelectedValue), tbProduitNom.Text, double.Parse(tbProduitPU.Text), byteImage, int.Parse(tbProduitNombre.Text), dateExpitation.Value))
                {
                    MessageBox.Show("Produit ajouter avec succes", "Produit", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    (produitSection as ProduitsForm).AfficherListeProduitsDisponible();
                }
                else
                {
                    MessageBox.Show("Produit existe deja", "Produit", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
              
                this.Close();
            }
            else
            {
                MemoryStream MS = new MemoryStream();
                imageProduit.Image.Save(MS, imageProduit.Image.RawFormat);
                byte[] byteImage = MS.ToArray();
                Objets.ProduitBD ClsProduit = new Objets.ProduitBD();
                DialogResult RS = MessageBox.Show("Voulez-vous vraiment modifier le produit", "Modification", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (RS == DialogResult.Yes)
                {
                    ClsProduit.ModifierProduit(IDProduit, Convert.ToInt32(cbProduitCategorie.SelectedValue), tbProduitNom.Text, double.Parse(tbProduitPU.Text), byteImage, int.Parse(tbProduitNombre.Text), dateExpitation.Value);
                    MessageBox.Show("Produit modifié avec succes", "Modification", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    (produitSection as ProduitsForm).AfficherListeProduitsDisponible();
                    Close();
                }else
                {
                    MessageBox.Show("Modification annulé", "Modification", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                   
                }
                this.Close();
            }
        }

       
    }
}

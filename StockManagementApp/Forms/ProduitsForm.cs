﻿using StockManagementApp.Objets;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace StockManagementApp.Forms
{
    public partial class ProduitsForm : MetroFramework.Forms.MetroForm
    {

        static ProduitsForm produitSection;
        public static ProduitsForm Instance
        {
            get
            {
                if (produitSection == null)
                    produitSection = new ProduitsForm();
                return produitSection;
            }
        }
        private StockEntities DB;

        public ProduitsForm()
        {
            InitializeComponent();
            DB = new StockEntities();
        }

        public void AfficherListeProduitsDisponible()
        {
            DB = new StockEntities();
            gridViewProduits.Rows.Clear();
            Categorie catg = new Categorie();
            foreach (var prod in DB.Produit)
            {
                catg = DB.Categorie.SingleOrDefault(c => c.Id == prod.Categorie_Id);
                if (catg != null)
                {
                    if (prod.Date_Expiration >= DateTime.Now && prod.Nombre !=0)
                    {
                        gridViewProduits.Rows.Add(false, prod.Id, prod.Nom, prod.Prix_Unitaire, prod.Nombre, prod.Date_Expiration, catg.Nom,"Disponible");     
                    }
                
                }
            }
            
        }

        private void btAjouterProduit_Click(object sender, EventArgs e)
        {
            AjouterProduitForm form = new AjouterProduitForm(this);
            form.ShowDialog();
           
        }

        private void ProduitsForm_Load(object sender, EventArgs e)
        {
            AfficherListeProduitsDisponible();
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            Produit PR = new Produit();
            if (SelectVerif() != null)
            {
                MessageBox.Show(SelectVerif(), "Modification", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }else
            {
                AjouterProduitForm form = new AjouterProduitForm(this);
                form.LabelTitre.Text = "Modifier Produit";
                form.btAnnuler.Visible = false;
                for (int i=0;i<gridViewProduits.Rows.Count;i++)
                {
                    if ((bool)gridViewProduits.Rows[i].Cells[0].Value==true)
                    {
                        int MYIDSELECT = (int)gridViewProduits.Rows[i].Cells[1].Value;
                        PR = DB.Produit.SingleOrDefault(s => s.Id == MYIDSELECT);
                        if (PR != null)
                        { 
                            form.cbProduitCategorie.Text = gridViewProduits.Rows[i].Cells[6].Value.ToString();                      
                            form.tbProduitNom.Text = gridViewProduits.Rows[i].Cells[2].Value.ToString();
                            form.tbProduitPU.Text = gridViewProduits.Rows[i].Cells[3].Value.ToString();
                            form.tbProduitNombre.Text = gridViewProduits.Rows[i].Cells[4].Value.ToString();
                            form.dateExpitation.Text = gridViewProduits.Rows[i].Cells[5].Value.ToString();
                            form.IDProduit = (int)gridViewProduits.Rows[i].Cells[1].Value;
                            MemoryStream MS = new MemoryStream(PR.Poduit_Image);
                            form.imageProduit.Image = Image.FromStream(MS);
                        }
                    }
                }
                
            form.ShowDialog();
            }
          
        }

       

        public string SelectVerif()
        {
            int NombreligneSelect = 0;
            for(int i=0;i< gridViewProduits.Rows.Count; i++)
            {
                if ((bool)gridViewProduits.Rows[i].Cells[0].Value == true)
                {
                    NombreligneSelect++;
                }
                
            }

            if (NombreligneSelect == 0)
            {
                return "Selectionner un produit";
            }
            if (NombreligneSelect > 1)
            {
                ActualiserData();
                return "Selectionner seulement 1 produit";
            }
            return null;
        }

        public void ActualiserData()
        {
            DB = new StockEntities();
            gridViewProduits.Rows.Clear();
            Categorie cat = new Categorie();
            foreach(var Lis in DB.Produit)
            {
                cat = DB.Categorie.SingleOrDefault(s => s.Id == Lis.Categorie_Id);
                if (cat != null)
                {
                    if (Lis.Date_Expiration <= DateTime.Now || Lis.Nombre == 0)
                    {
                        gridViewProduits.Rows.Add(false, Lis.Id, Lis.Nom, Lis.Prix_Unitaire, Lis.Nombre, Lis.Date_Expiration, cat.Nom, "Non Disponible");
                    }
                    else if (Lis.Date_Expiration > DateTime.Now && Lis.Nombre != 0)
                    {
                        gridViewProduits.Rows.Add(false, Lis.Id, Lis.Nom, Lis.Prix_Unitaire, Lis.Nombre, Lis.Date_Expiration, cat.Nom, "Disponible");
                    }        
                }
            }

        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            if(SelectVerif()== "Selectionner un produit")
            {
                MessageBox.Show(SelectVerif(), "Suppression", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }else
            {
                DialogResult DR = MessageBox.Show("Voulez-vous vraiment supprimer", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(DR==DialogResult.Yes)
                {
                    for(int i=0;i<gridViewProduits.Rows.Count;i++)
                    {
                        if((bool)gridViewProduits.Rows[i].Cells[0].Value==true)
                        {
                            ProduitBD produit = new ProduitBD();
                            int idselect = (int)gridViewProduits.Rows[i].Cells[1].Value;
                            produit.SupprimerProduit(idselect);
                        }
                    }

                    ActualiserData();
                    MessageBox.Show("Produit supprimer avec succes", "Suppression", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }else
                {
                    MessageBox.Show("Suppression est annulé", "Suppression", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }
      
        private void textSearch_Enter(object sender, EventArgs e)
        {
            if (textSearch.Text == "Rechercher")
            {
                textSearch.Text = "";
                textSearch.ForeColor = Color.Black;
            }
        }

        private void textSearch_TextChanged(object sender, EventArgs e)
        {
            label1.Text = "Rechercher Un Produit Par Nom";
            gridViewProduits.Rows.Clear();
            DB = new StockEntities();
            var listeRecherche = DB.Produit.ToList();
            listeRecherche = listeRecherche.Where(s => s.Nom.IndexOf(textSearch.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
           

            Categorie cat = new Categorie();

            foreach (var l in listeRecherche)
            {
                cat = DB.Categorie.SingleOrDefault(s => s.Id == l.Categorie_Id);
                if (l.Date_Expiration <= DateTime.Now || l.Nombre == 0)
                {
                    gridViewProduits.Rows.Add(false, l.Id, l.Nom, l.Prix_Unitaire, l.Nombre, l.Date_Expiration, cat.Nom, "Non Disponible");
                }
                else if (l.Date_Expiration > DateTime.Now && l.Nombre != 0)
                {
                    gridViewProduits.Rows.Add(false, l.Id, l.Nom, l.Prix_Unitaire, l.Nombre, l.Date_Expiration, cat.Nom, "Disponible");
                }
               
            }
        }

        private void btnDestocke_Click(object sender, EventArgs e)
        {
            label1.Text = "Liste Des Produit Destocké";
            Categorie catg = new Categorie();
            gridViewProduits.Rows.Clear();
            foreach (var prod in DB.Produit)
            {

                catg = DB.Categorie.SingleOrDefault(c => c.Id == prod.Categorie_Id);


                if (catg != null)
                {
                    if (prod.Date_Expiration <= DateTime.Now || prod.Nombre == 0)
                    {
                        gridViewProduits.Rows.Add(false, prod.Id, prod.Nom, prod.Prix_Unitaire, prod.Nombre, prod.Date_Expiration, catg.Nom, "Non Disponible");
                    }
                                       
                }
            }
        }

        private void gridViewProduits_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            FontDialog fd = new FontDialog();
            fd.ShowColor = true;
            Produit PR = new Produit();
            int row_index = e.RowIndex;
      
            for (int i = 0; i < gridViewProduits.Rows.Count; i++)
            {
                if (row_index != i)
                {
                    gridViewProduits.Rows[i].Cells["Column1"].Value = false;
                }

            }
            if (e.ColumnIndex == gridViewProduits.Columns["Column1"].Index)
            {
                try
                {
                    //  Block of code to try
                    gridViewProduits.EndEdit();
                    if ((bool)gridViewProduits.Rows[e.RowIndex].Cells["Column1"].Value)
                    {
                        AjouterProduitForm form = new AjouterProduitForm(this);
                        form.LabelTitre.Text = "Afficher Produit";
                        form.btAnnuler.Visible = false;
                        form.btEnregistrer.Visible = false;
                        form.btParcourir.Visible = false;

                        int MYIDSELECT = (int)gridViewProduits.SelectedRows[0].Cells[1].Value;
                        PR = DB.Produit.SingleOrDefault(s => s.Id == MYIDSELECT);

                        form.cbProduitCategorie.Text = gridViewProduits.SelectedRows[0].Cells[6].Value.ToString();
                        form.cbProduitCategorie.Enabled = false;
                        form.tbProduitNom.Text = gridViewProduits.SelectedRows[0].Cells[2].Value.ToString();
                        form.tbProduitNom.Enabled = false;
                        form.tbProduitPU.Text = gridViewProduits.SelectedRows[0].Cells[3].Value.ToString();
                        form.tbProduitPU.Enabled = false;
                        form.tbProduitNombre.Text = gridViewProduits.SelectedRows[0].Cells[4].Value.ToString();
                        form.tbProduitNombre.Enabled = false;
                        form.dateExpitation.Text = gridViewProduits.SelectedRows[0].Cells[5].Value.ToString();
                        form.dateExpitation.Enabled = false;
                        MemoryStream MS = new MemoryStream(PR.Poduit_Image);
                        form.imageProduit.Image = Image.FromStream(MS);
                        form.ShowDialog();

                    }

                }
            
                catch (Exception a)
                {
                    //  Block of code to handle errors            
                }
            

            }
        }

        private void btnListProduit_Click(object sender, EventArgs e)
        {
            label1.Text = "Liste De Tous Les Produits";
            gridViewProduits.Rows.Clear();
            ActualiserData();
        }

     
    }
}
